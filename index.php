<?php

require __DIR__ . '/vendor/autoload.php'; 

class App
{
    public $diveFitParser;

    public function __construct($file)
    {
        try {
            $this->diveFitParser = (new DiveFitParser\DiveFitParser($file));
        } catch (Exception $e) {
            throw new Exception('Not a valid fit file');
        }           
    }    

    public static function isFitFile($file) {
        try {
            self::$diveFitParser = (new DiveFitParser\DiveFitParser($file));
            return !is_null(self::$diveFitParser);
        } catch(\Exception $e) {
            return false;
        }
    }

    public function parseFitFile() {
        $log = [
            'device_model'        => $this->diveFitParser->getDeviceModel(),
            'start_time'          => $this->diveFitParser->getLogStartTime(),
            'max_depth'           => $this->diveFitParser->getMaxDepth(), // given in meters
            'bottom_time'         => $this->diveFitParser->getBottomTime(), // given in seconds
            // 'note'                => '',
            // 'start_tank_pressure' => '', // given in pascal
            // 'end_tank_pressure'   => '', // given in pascal
            // 'water_temperature'   => '', // given in kelvin
            // 'weight'              => '', // given in kilograms
            // 'visibility'          => '',
            // 'current'             => '',
            // 'unit_system'         => '', // default to metric = 0
            // 'date'                => ''
        ];
        
        var_dump($log);
    }

    public function debugInfo() {
        $data_mesgs = $this->diveFitParser->getMessages();
        echo "<pre>";
        var_dump($data_mesgs);
        echo "</pre>";        
    }
}

$file = './fit_files/my_fit_file.fit';
(new App($file))->parseFitFile();



