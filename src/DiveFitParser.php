<?php 
namespace DiveFitParser;

use adriangibbons\phpFITFileAnalysis;
use \Exception;

class DiveFitParser 
{
    private $_pFFA;

    public function __construct($file)
    {
        try {
            $this->setParser($file);
        } catch (Exception $e) {
            throw new Exception('Not a valid fit file');
        }           
    }

    public function setParser ($file) {
        if (!$this->_pFFA) {
            $options = [
                // Just using the defaults so no need to provide
                //		'fix_data'	=> [],
                		'units'		=> 'metric',
                //		'pace'		=> false
            ];            
            $this->_pFFA = new phpFITFileAnalysis($file, $options);
        }
    }

    public function getParser() {
        return $this->_pFFA;
    }

    public function getMessages() {
        return $this->_pFFA->data_mesgs;
    }

    public function getDeviceModel() {
        return $this->_pFFA->manufacturer();        
    }

    public function getLogStartTime() {
        return date('H:i:s', $this->_pFFA->data_mesgs['session']['start_time']);
    }

    public function getMaxDepth() {
        return is_array($this->_pFFA->data_mesgs['dive_summary']['max_depth']) ? 
                        $this->_pFFA->data_mesgs['dive_summary']['max_depth'][0] : 
                        $this->_pFFA->data_mesgs['dive_summary']['max_depth'];
    }

    public function getBottomTime() {
        var_dump($this->_pFFA->data_mesgs['device_info']['manufacturer']);
        var_dump($this->_pFFA->data_mesgs['dive_summary']['reference_mesg']);
        var_dump($this->_pFFA->data_mesgs['dive_summary']['reference_index']);
        return 0;
    }
}